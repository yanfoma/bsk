<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth'], function () {

    Route::get('mon-espace',[
        'uses' => 'StudentController@dashboard',
        'as'   => 'student.dashboard'
    ]);

    Route::get('mon-profil',[
        'uses' => 'StudentController@profile',
        'as'   => 'student.profile'
    ]);

    Route::get('votre-panier',[
        'uses' => 'HomeController@cart',
        'as'   => 'cart.index'
    ]);

    Route::get('ajouter-dans-le-panier/{id}',[
        'uses' => 'HomeController@addToCart',
        'as'   => 'cart.addToCart'
    ]);

    Route::get('votre-commande',[
        'uses' => 'HomeController@checkoutMethods',
        'as'   => 'cart.checkoutMethods'
    ]);

    Route::get('payer-avec-paypal',[
        'uses' => 'HomeController@checkoutPaypal',
        'as'   => 'cart.checkoutPaypal'
    ]);

//    Route::post('passer-votre-commande',[
//        'uses' => 'HomeController@order',
//        'as'   => 'cart.order'
//    ]);

    Route::get('passer-votre-commande',[
        'uses' => 'HomeController@paymentStore',
        'as'   => 'payment.store'
    ]);

    Route::get('transaction-reussie',[
        'uses' => 'HomeController@thanks',
        'as'   => 'thanks'
    ]);

    Route::get('votre-panier/supprimer/{id}',[
        'uses' => 'HomeController@cartDelete',
        'as'   => 'cart.delete'
    ]);

});

Route::group(['middleware' => 'guest'], function () {

    Route::get('/', [
        'uses' => 'HomeController@index',
        'as'   => 'home'
    ]);

    Route::get('accueil',[
        'uses' => 'HomeController@index',
        'as'   => 'home'
    ]);

    Route::get('toutes-les-formations',[
        'uses' => 'HomeController@courses',
        'as'   => 'courses'
    ]);


    Route::get('formations/{slug}',[
        'uses' => 'HomeController@singleCourse',
        'as'   => 'singleCourse'
    ]);

    Route::get('nous-contacter',[
        'uses' => 'HomeController@contact',
        'as'   => 'contact'
    ]);
});

Route::prefix('admin')->group(function(){

    Route::resource('series', 'SerieController');

});

Auth::routes();

Route::get(trans('/deconnection'),[
    'uses'=> 'Auth\LoginController@logout',
    'as'  => 'logout'
]);

//Route::get('se-connecter', ['as' => 'loginStudent', 'uses' => 'LoginController@loginStudent']);
//Route::group(['middleware' => 'auth', 'prefix' => 'etudiant'], function ()
//{
//
//});
