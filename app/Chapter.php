<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chapter extends Model
{
    protected $fillable = ['serie_id','title','slug'];

    public function lessons(){
        return $this->hasMany('App\Lesson');

    }

    public function serie(){
        return $this->belongsTo('App\Serie');

    }
}
