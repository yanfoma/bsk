<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Serie extends Model
{
    public function category(){
        return $this->belongsTo('App\Category');
    }

    public function lessons(){
        return $this->hasMany('App\Lesson');
    }

    public function chapters(){

        return $this->hasMany('App\Chapter');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function isPublic(){

        return  $this->isPublished == false ? true : false;
    }

    public function type(){

        return  $this->type == 'enSite' ? "Site" : "App";
    }
    public function isPublished(){

        return  $this->isPublished == false ? 'Privé' : 'Public';
    }
}

