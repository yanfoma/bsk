<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if($this->app->environment() === 'production'){
            $this->app['request']->server->set('HTTPS', true);
            //URL::forceScheme('https');
        }

        app()->setLocale(request()->segment(1));

        $locale=app()->getLocale();
        //Date::setLocale($locale);


        Schema::defaultStringLength(191);
    }

    public function register()
    {

    }
}
