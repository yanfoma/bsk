<?php

namespace App\Http\Controllers;

use App\Category;
use App\Payment;
use App\Serie;
use Cart;
use Illuminate\Support\Facades\Auth;
use Session;
use Srmklive\PayPal\Services\ExpressCheckout;
use Srmklive\PayPal\Services\AdaptivePayments;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index()
    {
        $series     = Serie::where('isPublished',0)->orderBy('created_at','desc')->take(12)->get();
        $categories = Category::orderBy('created_at','desc')->get();
        return view('index',compact('series','categories'));
    }
    public function singleCourse($slug)
    {
        $serie    = Serie::where('slug',$slug)->first();
        $relateds =  Serie::with(['category'])->take(3)->get();
        //dd($relateds);
        return view('singleCourse',compact('serie','categories','relateds'));
    }

    public function courses()
    {
        $series     = Serie::where('isPublished',0)->get();
        $series     = Serie::all();
        $categories = Category::has('series')->orderBy('created_at','desc')->get();
        return view('courses',compact('series','categories'));
    }

    public function contact()
    {
        return view('contact');
    }

    public function cart()
    {
        return view('cart');
    }

    public function checkoutMethods()
    {
        return view('checkout');
    }

    public function addToCart($id){
        $serie = Serie::find($id);

        $cartItem = Cart::add([
            'id'    => $serie->id,
            'name'  => $serie->title,
            'qty'   => 1,
            'price' => $serie->price,
            'image' => $serie->image_url,
            'category' => $serie->category->title
        ]);

        Cart::associate($cartItem->rowId,'App\Serie');
        return redirect()->route('cart.index');
    }

    public function cartDelete($id)
    {
        Cart::remove($id);
        return redirect()->back();
    }

    public function checkoutPaypal()
    {
        if(Cart::content()->count() == 0)
        {
            Session::flash('info','Votre panier est vide. Veuillez ajouter des formations!!!');
            return redirect()->back();
        }else
        {
            $provider       = new ExpressCheckout;      // To use express checkout.
            $invoiceId      = uniqid();
            $data           = $this->cartData($invoiceId);
            //$response = $provider->setExpressCheckout($data);
            $response = $provider->setExpressCheckout($data);
            return redirect($response['paypal_link']);
            //give a discount of 10% of the order amount
            //$data['shipping_discount'] = round((10 / 100) * $total, 2);
        }

    }

    public function paymentStore(Request $request){

        $provider   = new ExpressCheckout;      // To use express checkout.
        $token      = $request->token;
        $PayerID    = $request->PayerID;
        $response   = $provider->getExpressCheckoutDetails($token);
        $invoiceId  = $response['INVNUM']==0 ? 0 : uniqid();
        $data       = $this->cartData($invoiceId);
        $response   = $provider->doExpressCheckoutPayment($data, $token, $PayerID);
        foreach (Cart::content() as $key=>$cart)
        {
            Payment::create([
                'serie_id' => $cart->id,
                'user_id'  => Auth::user()->id,
                'invoiceId' => $invoiceId
            ]);
        }
        Cart::destroy();
        return redirect()->route('student.dashboard');
    }

    protected function cartData($invoiceId){
        $data = [];
        $data['items'] = [];

        foreach (Cart::content() as $key=>$cart)
        {
            $itemDetails=[
                'name'  => $cart->title,
                'price' => $cart->price,
                'qty'   => 1
            ];
            $data['items'][]=$itemDetails;
        }
        $data['invoice_id'] = $invoiceId;
        $data['invoice_description'] = "Formation sur BSK";
        $data['return_url'] = route('payment.store');
        $data['cancel_url'] = route('cart.index');
        $total = 0;
        foreach($data['items'] as $item) {
            $total += $item['price']*$item['qty'];
        }
        $data['total'] = $total;
        return $data;
    }

}
