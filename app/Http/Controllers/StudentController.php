<?php

namespace App\Http\Controllers;

use App\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StudentController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function dashboard(){
        $formations = Payment::where('user_id',Auth::user()->id)->get();
        $tabName = 'cours';
        return view('students.dashboard',compact('formations','tabName'));
    }

    public function profile(){
        $formations = Payment::where('user_id',Auth::user()->id)->get();
        $tabName = 'profile';
        return view('students.dashboard',compact('formations','tabName'));
    }
}
