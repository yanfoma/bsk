<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = ['serie_id','user_id','invoiceId'];

    public function users()
    {
        return $this->hasMany(\App\User::class);
    }

    public function serie()
    {
        return Serie::find($this->id);
    }

}
