@extends('layouts.app')
@section('title')
    Se Connecter
@endsection
@section('content')
    <section class="login_area" style="padding: 20px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2">
                    <form method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <div class="cardify login">
                            <h3 class="text-center">Connection</h3>
                            <div class="login--form">
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email">Addresse E-Mail</label>
                                    <input id="email" type="email" class="text_field form-control" name="email" value="{{ old('email') }}" required autofocus>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password">Mot De Passe</label>
                                    <input id="password" type="password" class="form-control" name="password" required>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <div class="custom_checkbox">
                                        <input type="checkbox" id="ch2" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                        <label for="ch2">
                                            <span class="shadow_checkbox"></span>
                                            <span class="label_text">Se Souvenir De Moi</span>
                                        </label>
                                    </div>
                                </div>
                                <button class="btn btn--md btn-primary" type="submit">Connection</button>
                                <div class="login_assist">
                                    <p class="recover">
                                        <a href="{{ route('register') }}" style="padding-right: 30%;">Créer Un Compte</a>
                                        <a href="{{ route('password.request') }}">Mot de Passe Oublié?</a>
                                    </p>
                                </div>
                            </div>
                            <!-- end .login--form -->
                        </div>
                        <!-- end .cardify -->
                    </form>
                </div>
                <!-- end .col-md-6 -->
            </div>
            <!-- end .row -->
        </div>
        <!-- end .container -->
    </section>
@endsection
