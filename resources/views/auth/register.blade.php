@extends('layouts.app')
@section('title')
    Créer Un Compte
@endsection
@section('content')
    <section class="signup_area " style="padding: 20px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <form method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        <div class="cardify signup_form">
                            <h5 class="text-center">Créer Un Compte</h5>
                            <hr>
                            <div class="login--form">
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="urname">Votre Nom</label>
                                    <input id="name" type="text" class="form-control text_field" name="name" value="{{ old('name') }}" required autofocus placeholder="Veuillez entrer votre Nom">
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email_ad">Addresse E-Mail </label>
                                    <input id="email" type="email" class="form-control text_field" name="email" value="{{ old('email') }}" required placeholder="Veuillez entrer votre addresse e-mail ">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password">Mot De Passe</label>
                                    <input id="password" type="password" class="form-control text_field" name="password" required placeholder="Veuillez entrer votre mot de passe">
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="password-confirm">Confirmer Mot de Passe</label>
                                    <input id="password-confirm" type="password" class="form-control text_field" name="password_confirmation" required placeholder="Veuillez confirmer votre mot de passe">
                                </div>

                                <button class="btn btn--md register_btn btn-primary" type="submit">Créer</button>

                                <div class="login_assist">
                                    <p>Vous avez déjà un compte?
                                        <a href="{{route('login')}}">Se Connecter</a>
                                    </p>
                                </div>
                            </div>
                            <!-- end .login--form -->
                        </div>
                        <!-- end .cardify -->
                    </form>
                </div>
                <!-- end .col-md-6 -->
            </div>
            <!-- end .row -->
        </div>
        <!-- end .container -->
    </section>
@endsection
