@extends('layouts.app')
@section('title')
    Mot De Passe Oublié
@endsection
@section('content')
    <section class="login_area section--padding" style="margin-bottom: 50px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                            {{ csrf_field() }}
                        <div class="cardify login">
                            <h3 class="text-center">Mot De Passe Oublié</h3>
                            <div class="login--form">
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email">Addresse E-Mail</label>
                                    <input id="email" type="email" class="text_field form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="veuillez saisir votre addresse email ici">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <button class="btn btn--md btn-primary" type="submit">Envoyer le lien de réinitialisation du mot de passe</button>
                            </div>
                            <!-- end .login--form -->
                        </div>
                        <!-- end .cardify -->
                    </form>
                </div>
                <!-- end .col-md-6 -->
            </div>
            <!-- end .row -->
        </div>
        <!-- end .container -->
    </section>
@endsection
