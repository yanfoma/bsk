@extends('layouts.app')

@section('title')
    Nous Contacter
@endsection

@section('style')
@endsection

@section('content')
    @include('layouts.contact')
@endsection
@section('post_header')
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" 		   content="YanfomaShop" />
    <meta property="og:site_name" 	   content="Yanfoma">
    <meta property="fb:app_id" 		   content="400025927061215">
    <meta property="og:title"          content="YanfomaShop" />
    <meta property="og:description"    content="YanfomaShop" />
    <meta property="og:image"          content="{{asset('images/frontEnd/YanfomaShop.png')}}" />
    <meta property="og:type" 	       content="article">
    <meta property="og:url"            content="https://yanfoma.tech">
@endsection

@section('scripts')
    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDxflHHc5FlDVI-J71pO7hM1QJNW1dRp4U"></script>
    <script src="{{asset('js/map.js')}}"></script>
@endsection
