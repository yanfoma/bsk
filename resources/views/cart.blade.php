@extends('layouts.app')

@section('title')
    BSK Panier
@endsection

@section('style')
@endsection

@section('content')
    <section class="cart_area section--padding bgcolor">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @if(Cart::content()->count())
                        <div class="product_archive added_to__cart">
                            <div class="faq-head">
                                <h2 class="primary text-center p-top-30 p-bottom-30">Votre Panier</h2>
                            </div>
                        <div class="table-responsive single_product">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col"><h4>Nom</h4></th>
                                        <th scope="col"><h4>Prix</h4></th>
                                        <th scope="col"><h4>Supprimer</h4></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach(Cart::content() as $serie)
                                        @php $formation = \App\Serie::find($serie->id); @endphp
                                        <tr>
                                        <td colspan="1">
                                            <div class="product__description">
                                                <div class="p_image"><img src="{{asset($formation->image_url)}}" width="100" height="70" alt="Purchase image"></div>
                                                <div class="short_desc">
                                                    <h6>{{$serie->name}}</h6>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="item_price">
                                                <span>{{$serie->price}} CFA</span>
                                            </div>
9   &²3                                        </td>
                                        <td>
                                            <div class="item_action">
                                                <a href="{{route('cart.delete', ['id' =>$serie->rowId])}}" class="remove_from_cart">
                                                    <span class="icon-trash"></span>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="cart_calculation text-right">
                                        <div class="cart--total">
                                            <p>
                                                <span>total</span>{{Cart::total()}} CFA</p>
                                        </div>
                                        <a href="{{route('courses')}}" class="m-left-50 btn btn--md checkout_link pull-left btn-primary">Ajouter Des Formations</a>
                                        <a href="{{route('cart.checkoutPaypal')}}" class="btn btn--md checkout_link btn-primary">Payer Avec Paypal</a>
                                    </div>
                                </div>
                            </div>
                    </div>
                    @else
                        <div class="col-lg-12 col-sm-12 m-top-100">
                            <div class="alert alert-primary text-center" role="alert">
                                <strong>Votre Panier Est Vide !!!</strong>
                            </div>
                            <div class="text-center m-top-30 m-bottom-100">
                                <a href="{{route('courses')}}" class="btn btn--lg btn-primary">Ajouter Formations</a>
                            </div>
                        </div>
                @endif
                    <!-- end /.product_archive2 -->
                </div>
                <!-- end .col-md-12 -->
            </div>
            <!-- end .row -->

        </div>
        <!-- end .container -->
    </section>

@endsection
@section('post_header')
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" 		   content="YanfomaShop" />
    <meta property="og:site_name" 	   content="Yanfoma">
    <meta property="fb:app_id" 		   content="400025927061215">
    <meta property="og:title"          content="YanfomaShop" />
    <meta property="og:description"    content="YanfomaShop" />
    <meta property="og:image"          content="{{asset('images/frontEnd/YanfomaShop.png')}}" />
    <meta property="og:type" 	       content="article">
    <meta property="og:url"            content="https://yanfoma.tech">
@endsection

@section('scripts')
@endsection
