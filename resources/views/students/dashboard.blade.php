@extends('layouts.app')

@section('content')
    <section class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12 breadcrumb-contents">
                    <h2 class="page-title">Bienvenue {{Auth::user()->name}}</h2>
                    <div class="breadcrumb">
                    </div>
                </div>
                <!-- end /.col-md-12 -->
            </div>
            <!-- end /.row -->
        </div>
        <!-- end /.container -->
    </section>
    <section class="author-profile-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="author-profile">
                                <div class="row">
                                    <div class="col-lg-5 col-md-7">
                                        <div class="author-desc">
                                            <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1539200258/Yanfoma/yanfoshop.png" alt="" width="100px">
                                            <div class="infos">
                                                <h4>{{Auth::user()->name}}</h4>
                                                <span>Membre Depuis: {{Auth::user()->created_at}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 order-lg-2 col-md-5 order-md-1">
                                        <div class="author-stats">
                                            <ul>
                                                <li class="t_items">
                                                    <span>{{$formations->count()}}</span>
                                                    <p>Vos Cours</p>
                                                </li>
                                                {{--<li class="t_sells">--}}
                                                    {{--<span>2426</span>--}}
                                                    {{--<p>Total Sales</p>--}}
                                                {{--</li>--}}
                                            </ul>
                                        </div>
                                        <span class="btn btn-sm btn-primary" aria-hidden="true">Changer</span>
                                        <a href="" class="btn btn-sm btn-danger">Delete Image</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 author-info-tabs">
                            <ul class="nav nav-tabs" id="author-tab" role="tablist">
                                <li>
                                    <a class="{{ empty($tabName) || $tabName == 'cours' ? 'active' : 'items-tab' }}" id="profile-tab" data-toggle="tab" href="#cours" role="tab" aria-controls="cours" aria-selected="true">Mes Cours</a>
                                </li>
                                <li>
                                    <a class="{{ empty($tabName) || $tabName == 'profile' ? 'active' : 'items-tab' }}" data-toggle="tab" href="#profile" role="tab" aria-controls="items" aria-selected="false">Mon Profile</a>
                                </li>

                            </ul>
                            <div class="tab-content" id="author-tab-content">
                                <div class="tab-pane fade show {{ empty($tabName) || $tabName == 'cours' ? 'active' : 'items-tab' }}" id="cours" role="tabpanel" aria-labelledby="items-tab">
                                    <section class="dashboard-area">
                                        <div class="dashboard_contents">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="filter-bar dashboard_title_area clearfix filter-bar2">
                                                            <div class="dashboard__title">
                                                                <h3>Mes Cours</h3>
                                                            </div>
                                                            <div class="filter__items">
                                                                <div class="filter__option filter--text">
                                                                    <p>
                                                                        <span>{{$formations->count()}}</span> Cours</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    @foreach ($formations as $serie)
                                                        <div class="col-lg-4 col-md-6">
                                                            <div class="product-single latest-single items--edit">
                                                                <div class="product-thumb">
                                                                    <figure>
                                                                        <img src="{{asset($serie->serie()->image_url)}}" alt="" class="img-fluid">
                                                                    </figure>
                                                                </div>
                                                                <div class="product-excerpt">
                                                                    <h5>
                                                                        <a href="">{{$serie->serie()->title}}</a>
                                                                    </h5>
                                                                    <ul class="titlebtm">
                                                                        <li>
                                                                            {{--<img class="auth-img" src="" alt="author image">--}}
                                                                            <p>
                                                                                <a href="#">{{$serie->serie()->user->name}}</a>
                                                                            </p>
                                                                        </li>
                                                                        <li class="product_cat">
                                                                            Dans
                                                                            <a href="#">{{$serie->serie()->category->name}}</a>
                                                                        </li>
                                                                    </ul>
                                                                    <div class="text-center m-top-50">
                                                                        <a href="#" class="btn btn--lg btn-primary">Commencer</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                                <div class="tab-pane fade show {{ empty($tabName) || $tabName == 'profile' ? 'active' : 'items-tab' }}" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                    <div class="dashboard_contents">
                                        <div class="container">
                                            <form action="#" class="setting_form">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="information_module">
                                                            <div class="toggle_title">
                                                                <h4>Mes Informations</h4>
                                                            </div>
                                                            <div class="information__set">
                                                                <div class="information_wrapper form--fields row">
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="acname">Nom
                                                                                <sup>*</sup>
                                                                            </label>
                                                                            <input type="text" id="acname" class="text_field" placeholder="First Name" value="{{Auth::user()->name}}">
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="usrname">Nom d'utilisateur
                                                                                <sup>*</sup>
                                                                            </label>
                                                                            <input type="text" id="usrname" class="text_field" placeholder="Account name" value="{{Auth::user()->name}}">
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="emailad">Addresse E-mail
                                                                                <sup>*</sup>
                                                                            </label>
                                                                            <input type="text" id="emailad" class="text_field" placeholder="Email address" value="{{Auth::user()->email}}">
                                                                        </div>
                                                                    </div>

                                                                    {{--<div class="col-md-6">--}}
                                                                        {{--<div class="form-group">--}}
                                                                            {{--<label for="website">Website</label>--}}
                                                                            {{--<input type="password" id="website" class="text_field" placeholder="Website">--}}
                                                                        {{--</div>--}}
                                                                    {{--</div>--}}

                                                                    {{--<div class="col-md-6">--}}
                                                                        {{--<div class="form-group">--}}
                                                                            {{--<label for="password">Password--}}
                                                                                {{--<sup>*</sup>--}}
                                                                            {{--</label>--}}
                                                                            {{--<input type="password" id="password" class="text_field" placeholder="Email address">--}}
                                                                        {{--</div>--}}
                                                                    {{--</div>--}}

                                                                    {{--<div class="col-md-6">--}}
                                                                        {{--<div class="form-group">--}}
                                                                            {{--<label for="conpassword">Confirm Password--}}
                                                                                {{--<sup>*</sup>--}}
                                                                            {{--</label>--}}
                                                                            {{--<input type="password" id="conpassword" class="text_field" placeholder="re-enter password">--}}
                                                                        {{--</div>--}}
                                                                    {{--</div>--}}

                                                                    {{--<div class="col-md-6">--}}
                                                                        {{--<div class="form-group">--}}
                                                                            {{--<label for="country">Country--}}
                                                                                {{--<sup>*</sup>--}}
                                                                            {{--</label>--}}
                                                                            {{--<div class="select-wrap select-wrap2">--}}
                                                                                {{--<select name="country" id="country" class="text_field">--}}
                                                                                    {{--<option value="">Select one</option>--}}
                                                                                    {{--<option value="bangladesh">Bangladesh</option>--}}
                                                                                    {{--<option value="india">India</option>--}}
                                                                                    {{--<option value="uruguye">Uruguye</option>--}}
                                                                                    {{--<option value="australia">Australia</option>--}}
                                                                                    {{--<option value="neverland">Neverland</option>--}}
                                                                                    {{--<option value="atlantis">Atlantis</option>--}}
                                                                                {{--</select>--}}
                                                                                {{--<span class="lnr icon-arrow-down"></span>--}}
                                                                            {{--</div>--}}
                                                                        {{--</div>--}}
                                                                    {{--</div>--}}

                                                                    {{--<div class="col-md-6">--}}
                                                                        {{--<div class="form-group">--}}
                                                                            {{--<label for="prohead">Profile Heading</label>--}}
                                                                            {{--<input type="text" id="prohead" class="text_field" placeholder="Ex: Webdesign &amp; Development Service">--}}
                                                                        {{--</div>--}}
                                                                    {{--</div>--}}

                                                                    {{--<div class="col-md-12">--}}
                                                                        {{--<div class="form-group">--}}
                                                                            {{--<label for="authbio">Author Bio</label>--}}
                                                                            {{--<textarea name="author_bio" id="authbio" class="text_field" placeholder="Short brief about yourself or your account..."></textarea>--}}
                                                                        {{--</div>--}}
                                                                    {{--</div>--}}
                                                                </div>
                                                                <!-- end /.information_wrapper -->
                                                            </div>
                                                        </div>
                                                    </div>

                                                    {{--<div class="col-md-12">--}}
                                                        {{--<div class="information_module">--}}
                                                            {{--<div class="toggle_title">--}}
                                                                {{--<h4>Billing Information</h4>--}}
                                                            {{--</div>--}}

                                                            {{--<div class="information__set">--}}
                                                                {{--<div class="information_wrapper form--fields">--}}
                                                                    {{--<div class="row">--}}
                                                                        {{--<div class="col-md-6">--}}
                                                                            {{--<div class="form-group">--}}
                                                                                {{--<label for="first_name">Full Name--}}
                                                                                    {{--<sup>*</sup>--}}
                                                                                {{--</label>--}}
                                                                                {{--<input type="text" id="first_name" class="text_field" placeholder="First Name" value="Ron">--}}
                                                                            {{--</div>--}}
                                                                        {{--</div><!-- ends: .col-md-6 -->--}}

                                                                        {{--<div class="col-md-6">--}}
                                                                            {{--<div class="form-group">--}}
                                                                                {{--<label for="email">Company Name--}}
                                                                                    {{--<sup>*</sup>--}}
                                                                                {{--</label>--}}
                                                                                {{--<input type="text" id="email" class="text_field" placeholder="AazzTech" value="AazzTech">--}}
                                                                            {{--</div>--}}
                                                                        {{--</div><!-- ends: .col-md-6 -->--}}

                                                                        {{--<div class="col-md-6">--}}
                                                                            {{--<div class="form-group">--}}
                                                                                {{--<label for="email1">Email Adress--}}
                                                                                    {{--<sup>*</sup>--}}
                                                                                {{--</label>--}}
                                                                                {{--<input type="text" id="email1" class="text_field" placeholder="Email address" value="contact@aazztech.com">--}}
                                                                            {{--</div>--}}
                                                                        {{--</div><!-- ends: .col-md-6 -->--}}

                                                                        {{--<div class="col-md-6">--}}
                                                                            {{--<div class="form-group">--}}
                                                                                {{--<label for="country1">Country--}}
                                                                                    {{--<sup>*</sup>--}}
                                                                                {{--</label>--}}
                                                                                {{--<div class="select-wrap select-wrap2">--}}
                                                                                    {{--<select name="country" id="country1" class="text_field">--}}
                                                                                        {{--<option value="">Select one</option>--}}
                                                                                        {{--<option value="bangladesh">Bangladesh</option>--}}
                                                                                        {{--<option value="india">India</option>--}}
                                                                                        {{--<option value="uruguye">Uruguye</option>--}}
                                                                                        {{--<option value="australia">Australia</option>--}}
                                                                                        {{--<option value="neverland">Neverland</option>--}}
                                                                                        {{--<option value="atlantis">Atlantis</option>--}}
                                                                                    {{--</select>--}}
                                                                                    {{--<span class="lnr icon-arrow-down"></span>--}}
                                                                                {{--</div>--}}
                                                                            {{--</div>--}}
                                                                        {{--</div><!-- ends: .col-md-6 -->--}}

                                                                        {{--<div class="col-md-6">--}}
                                                                            {{--<div class="form-group">--}}
                                                                                {{--<label for="address1">Address Line 1</label>--}}
                                                                                {{--<input type="text" id="address1" class="text_field" placeholder="Address line one">--}}
                                                                            {{--</div>--}}
                                                                        {{--</div><!-- ends: .col-md-6 -->--}}

                                                                        {{--<div class="col-md-6">--}}
                                                                            {{--<div class="form-group">--}}
                                                                                {{--<label for="address2">Address Line 2</label>--}}
                                                                                {{--<input type="text" id="address2" class="text_field" placeholder="Address line two">--}}
                                                                            {{--</div>--}}
                                                                        {{--</div><!-- ends: .col-md-6 -->--}}

                                                                        {{--<div class="col-md-6">--}}
                                                                            {{--<div class="form-group">--}}
                                                                                {{--<label for="city">City / State--}}
                                                                                    {{--<sup>*</sup>--}}
                                                                                {{--</label>--}}
                                                                                {{--<div class="select-wrap select-wrap2">--}}
                                                                                    {{--<select name="city" id="city" class="text_field">--}}
                                                                                        {{--<option value="">Select one</option>--}}
                                                                                        {{--<option value="dhaka">Dhaka</option>--}}
                                                                                        {{--<option value="sydney">Sydney</option>--}}
                                                                                        {{--<option value="newyork">New York</option>--}}
                                                                                        {{--<option value="london">London</option>--}}
                                                                                        {{--<option value="mexico">New Mexico</option>--}}
                                                                                    {{--</select>--}}
                                                                                    {{--<span class="lnr icon-arrow-down"></span>--}}
                                                                                {{--</div>--}}
                                                                            {{--</div>--}}
                                                                        {{--</div><!-- ends: .col-md-6 -->--}}

                                                                        {{--<div class="col-md-6">--}}
                                                                            {{--<div class="form-group">--}}
                                                                                {{--<label for="zipcode">Zip / Postal Code--}}
                                                                                    {{--<sup>*</sup>--}}
                                                                                {{--</label>--}}
                                                                                {{--<input type="text" id="zipcode" class="text_field" placeholder="zip/postal code">--}}
                                                                            {{--</div>--}}
                                                                        {{--</div><!-- ends: .col-md-6 -->--}}
                                                                    {{--</div>--}}
                                                                    {{--<!-- end /.row -->--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                            {{--<!-- end /.information__set -->--}}
                                                        {{--</div>--}}
                                                        {{--<!-- end /.information_module -->--}}
                                                    {{--</div>--}}
                                                    <div class="col-md-12">
                                                        <div class="information_module">
                                                            <div class="toggle_title">
                                                                <h4>Reseaux Sociaux</h4>
                                                            </div>
                                                            <div class="information__set social_profile">
                                                                <div class="information_wrapper">
                                                                    <div class="social__single">
                                                                        <div class="social_icon">
                                                                            <span class="fa fa-facebook"></span>
                                                                        </div>

                                                                        <div class="link_field">
                                                                            <input type="text" class="text_field" placeholder="ex: www.facebook.com/">
                                                                        </div>
                                                                    </div>
                                                                    <div class="social__single">
                                                                        <div class="social_icon">
                                                                            <span class="fa fa-twitter"></span>
                                                                        </div>
                                                                        <div class="link_field">
                                                                            <input type="text" class="text_field" placeholder="ex: www.twitter.com/">
                                                                        </div>
                                                                    </div>
                                                                    <div class="social__single">
                                                                        <div class="social_icon">
                                                                            <span class="fa fa-google-plus"></span>
                                                                        </div>
                                                                        <div class="link_field">
                                                                            <input type="text" class="text_field" placeholder="ex: www.google.com/">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="dashboard_setting_btn">
                                                            <button type="submit" class="btn btn--md btn-primary">Sauvegarder</button>
                                                            <button type="reset" class="btn btn-md btn-danger">Annuler</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
