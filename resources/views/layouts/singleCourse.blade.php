<section class="single-product-desc">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-md-12">
				<div class="item-preview">
					<img src="{{asset($serie->image_url)}}" alt="" class="img-fluid">
					<div class="item__preview-thumb">
						<div class="item-action border-none">
							<div class="action-btns">
								<a href="#" class="btn btn--lg btn-primary">Live Preview</a>
								<a href="{{route('cart.addToCart',['id' => $serie->id])}}" class="btn btn--lg btn--icon btn btn--border btn-secondary">
									<span class="lnr icon-basket"></span>Ajouter Au Panier</a>
							</div>
						</div>
						<!-- end /.item__action -->
						<div class="col-lg-12 col-md-12 p-top-50 p-bottom-0">
							<div class="faq-head">
								<h3 class="primary">{{$serie->title}}</h3>
							</div>
						</div>
						<div class="item-activity">
							<div class="activity-single">
								<p>
									<span class="icon-folder-alt"></span> Chapitre(s)
								</p>
								<p>{{$serie->chapters->count()}}</p>
							</div>
							<div class="activity-single">
								<p>
									<span class="icon-docs"> Cour(s)</span>
								</p>
								<p>{{$serie->lessons->count()}}</p>
							</div>
							<div class="activity-single">
								<p>
									<span class="icon-people"></span>Etudiants
								</p>
								<p>{{$serie->lessons->count()}}</p>
							</div>
						</div>
					</div>
				</div>

				<div class="item-info">
					<div class="item-navigation">
						<ul class="nav nav-tabs" role="tablist">
							<li>
								<a href="#product-details" class="active show" id="tab1" aria-controls="product-details" role="tab" data-toggle="tab" aria-selected="true">
									<span class="icon icon-docs"></span> Table De Content</a>
							</li>
							<li>
								<a href="#product-comment" id="tab2" aria-controls="product-comment" role="tab" data-toggle="tab" class="" aria-selected="false">
									<span class="icon icon-bubbles"></span> Description </a>
							</li>
							<li>
								<a href="#product-review" id="tab3" aria-controls="product-review" role="tab" data-toggle="tab">
									<span class="icon icon-info"></span> Prerequis
								</a>
							</li>
						</ul>
					</div>
					<div class="tab-content">
						<div class="fade tab-pane product-tab active show" id="product-details" role="tabpanel" aria-labelledby="tab1">
							<div class="tab-content-wrapper">
								<div class="support__title">
									<h3>Table de Content</h3>
								</div>
								<div class="panel-group accordion" role="tablist" id="accordion">
									@forelse($serie->chapters as $chapter)
									<div class="panel accordion__single" id="panel-one">
										<div class="single_acco_title">
											<h4>
												<a data-toggle="collapse" href="#collapse{{$chapter->id}}" class="collapsed" aria-expanded="false" data-target="#collapse{{$chapter->id}}" aria-controls="collapse{{$chapter->id}}">
													<span>{{$chapter->title}}</span>
													<i class="lnr icon-arrow-right-circle indicator"></i>
												</a>
											</h4>
										</div>

										<div id="collapse{{$chapter->id}}" class="panel-collapse collapse" aria-labelledby="panel-one" data-parent="#accordion">
											<div class="panel-body">
												<p>
													<ol style="padding-left: 50px;">
														@forelse($chapter->lessons as $lesson)
															<li style="padding: 20px;"><a href="">{{$lesson->title}}</a></li>
														@empty
															<h2>Aucun Cours</h2>
														@endforelse
													</ol>
												</p>
											</div>
										</div>
									</div>
									@empty
										<p>Aucun Chapitre</p>
									@endforelse
								</div>
							</div>
						</div>

						<div class="fade tab-pane product-tab" id="product-comment" role="tabpanel" aria-labelledby="tab2">
							<div class="support">
								<div class="support__title">
									<h3>Description</h3>
								</div>
								<div class="support__form">
									<div class="usr-msg">
										<p>{{$serie->description}}</p>
									</div>
								</div>
							</div>
						</div>

						<div class="fade tab-pane product-tab" id="product-review" role="tabpanel" aria-labelledby="tab3">
							<div class="support">
								<div class="support__title">
									<h3>Prerequis</h3>
								</div>
								<div class="support__form">
									<div class="usr-msg">
										<p>{{$serie->requirements}}</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-lg-4 col-md-12">
				<aside class="sidebar sidebar--single-product">
					<div class="sidebar-card card-pricing card--pricing2">
						<div class="price border-none">
							<h1>
								<span>{{$serie->price}}</span>
								<sup>CFA</sup>
							</h1>
						</div>

						<div class="purchase-button">
							<a href="{{route('cart.addToCart',['id' => $serie->id])}}" class="btn btn--lg cart-btn btn-secondary">
								<span class="lnr icon-basket"></span> Ajouter Au Panier</a>
						</div>
					</div>

					<div class="sidebar-card card--product-infos">
						{{--<div class="card-title text-center">--}}
							{{--<h4>Product Information</h4>--}}
						{{--</div>--}}

						{{--<ul class="infos">--}}
							{{--<li>--}}
								{{--<p class="data-label">Version</p>--}}
								{{--<p class="info">1.2</p>--}}
							{{--</li>--}}
							{{--<li>--}}
								{{--<p class="data-label">Category</p>--}}
								{{--<p class="info">Corporate &amp; Business</p>--}}
							{{--</li>--}}
							{{--<li>--}}
								{{--<p class="data-label">Layout</p>--}}
								{{--<p class="info">Responsive</p>--}}
							{{--</li>--}}
							{{--<li>--}}
								{{--<p class="data-label">Retina Ready</p>--}}
								{{--<p class="info">No</p>--}}
							{{--</li>--}}
							{{--<li>--}}
								{{--<p class="data-label">Files Included</p>--}}
								{{--<p class="info">Html, CSS, JavaScript</p>--}}
							{{--</li>--}}
							{{--<li>--}}
								{{--<p class="data-label">Browser</p>--}}
								{{--<p class="info">IE10, IE11, Firefox, Safari, Opera, Chrome5</p>--}}
							{{--</li>--}}
							{{--<li>--}}
								{{--<p class="data-label">Bootstrap</p>--}}
								{{--<p class="info">Bootstrap 4</p>--}}
							{{--</li>--}}
							{{--<li>--}}
								{{--<p class="data-label">Tags</p>--}}
								{{--<p class="info">--}}
									{{--<a href="#">business</a>,--}}
									{{--<a href="#">template</a>,--}}
									{{--<a href="#">onepage</a>,--}}
									{{--<a href="#">creative</a>,--}}
									{{--<a href="#">responsive</a>,--}}
									{{--<a href="#">corporate</a>,--}}
									{{--<a href="#">Bootstrap3</a>,--}}
									{{--<a href="#">html5</a>--}}
								{{--</p>--}}
							{{--</li>--}}
						{{--</ul>--}}
					</div>
					<div class="author-card sidebar-card ">
						<div class="card-title text-center">
							<h4>Partager</h4>
						</div>
						<div class="author-infos">
							<div class="social social--color--filled">
								<ul>
									<li>
										<a href="#">
											<span class="fa fa-facebook"></span>
										</a>
									</li>
									<li>
										<a href="#">
											<span class="fa fa-twitter"></span>
										</a>
									</li>
									<li>
										<a href="#">
											<span class="fa fa-linkedin"></span>
										</a>
									</li>
									<li>
										<a href="#">
											<span class="fa fa-google-plus"></span>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="author-card sidebar-card ">
						<div class="card-title text-center">
							<h4>Formateur</h4>
						</div>

						<div class="author-infos">
							<div class="author-top">
								<div class="author_avatar">
									<img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1539200258/Yanfoma/yanfoshop.png" alt="Presenting the broken author avatar :D">
								</div>
								<div class="author">
									<h5>{{$serie->user->name}}</h5>
									<p>Formation(s):{{$serie->user->series->count()}}</p>
								</div>
							</div>
							<br>
							{{--<div class="purchase-button">--}}
								{{--<a href="#" class="btn btn--lg btn-primary">Voir Le Profile</a>--}}
							{{--</div>--}}
						</div>
					</div>
				</aside>
			</div>
		</div>
	</div>
</section>