<section class="more_product_area p-top-100 p-bottom-70">
	<div class="container">
		<div class="row">
			<!-- start col-md-12 -->
			<div class="col-md-12">
				<div class="section-title">
					<h2>Formations Similaires
						{{--<span class="highlighted">Aazztech</span>--}}
					</h2>
				</div>
			</div>
			@forelse($relateds as $related)
			<div class="col-lg-4 col-md-6">
				<div class="product-single latest-single">
					<div class="product-thumb">
						<figure>
							<img src="images/product1.png" alt="" class="img-fluid">
							<figcaption>
								<ul class="list-unstyled">
									<li>
										<a href="">
											<span class="icon-basket"></span>
										</a>
									</li>
									<li>
										<a href="">Live Demo</a>
									</li>
								</ul>
							</figcaption>
						</figure>
					</div>
					<!-- Ends: .product-thumb -->
					<div class="product-excerpt">
						<h5>
							<a href="">E-commerce Shopping Cart</a>
						</h5>
						<ul class="titlebtm">
							<li>
								<img class="auth-img" src="images/auth-img2.png" alt="author image">
								<p>
									<a href="#">Theme-Valley</a>
								</p>
							</li>
							<li class="product_cat">
								in
								<a href="#">WordPress</a>
							</li>
						</ul>
						<ul class="product-facts clearfix">
							<li class="price">$20</li>
							<li class="sells">
								<span class="icon-basket"></span>81
							</li>
							<li class="product-fav">
								<span class="icon-heart" title="" data-toggle="tooltip" data-original-title="Add to collection"></span>
							</li>
							<li class="product-rating">
								<ul class="list-unstyled">
									<li class="stars">
                                            <span>
                                                <i class="fa fa-star"></i>
                                            </span>
										<span>
                                                <i class="fa fa-star"></i>
                                            </span>
										<span>
                                                <i class="fa fa-star"></i>
                                            </span>
										<span>
                                                <i class="fa fa-star"></i>
                                            </span>
										<span>
                                                <i class="fa fa-star"></i>
                                            </span>
									</li>
								</ul>
							</li>
						</ul>
					</div>
					<!-- Ends: .product-excerpt -->
				</div>
				<!-- Ends: .product-single -->
			</div>
			@empty
			@endforelse
		</div>
	</div>
</section>