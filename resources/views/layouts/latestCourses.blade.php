@if($series->count())
<section class="latest-product section--padding bgcolor">
	<div class="container">
		<div class="row">
			<!-- Start Section Title -->
			<div class="col-md-12">
				<div class="section-title">
					<h1>Nouvelles Formations</h1>
				</div>
			</div>
			<!-- Ends: .col-md-12/Section Title -->
			<!-- Start .product-list -->
			<div class="col-md-12 product-list">
				<div class="row">
					<!-- Start .col-md-4 -->
					@forelse($series as $serie)
						<div class="col-lg-4 col-md-6">
							<div class="product-single latest-single single--vendor">
								<div class="product-thumb">
									<figure>
										<img src="{{$serie->image_url}}" alt="" class="img-fluid">
										<figcaption>
											<ul class="list-unstyled">
												<li>
													<a href="{{route('cart.addToCart',['id' => $serie->id])}}">
														<span class="icon-basket"></span>
													</a>
												</li>
												<li>
													<a href="{{route('singleCourse', ['slug' =>$serie->slug])}}">Voir Plus</a>
												</li>
											</ul>
										</figcaption>
									</figure>
								</div>
								<!-- Ends: .product-thumb -->
								<div class="product-excerpt">
									<h5>
										<a href="">{{$serie->title}}</a>
									</h5>
									<ul class="titlebtm">
										<li>
											<img class="auth-img" src="{{asset('images/auth-img3.png')}}" alt="author image">
											<p>
												<a href="#"> Par : {{$serie->user->name}} </a>
											</p>
										</li>
										<li class="product_cat">
											 dans
											<a href="#">{{$serie->category->name}}</a>
										</li>
										<li class="product_cat">
											<a href="#"> {{$serie->chapters->count()}}</a>
											Chapitre(s)
										</li>
									</ul>
									<ul class="product-facts clearfix">
										<li class="price">{{$serie->price}} CFA</li>
										<li class="price">{{$serie->type()}}</li>
										{{--<li class="sells">--}}
											{{--<span class="icon-basket"></span>81--}}
										{{--</li>--}}
										{{--<li class="product-fav">--}}
											{{--<span class="icon-heart" title="Add to collection" data-toggle="tooltip"></span>--}}
										{{--</li>--}}
										{{--<li class="product-rating">--}}
											{{--<ul class="list-unstyled">--}}
												{{--<li class="stars">--}}
                                                    {{--<span>--}}
                                                        {{--<i class="fa fa-star"></i>--}}
                                                    {{--</span>--}}
													{{--<span>--}}
                                                        {{--<i class="fa fa-star"></i>--}}
                                                    {{--</span>--}}
													{{--<span>--}}
                                                        {{--<i class="fa fa-star"></i>--}}
                                                    {{--</span>--}}
													{{--<span>--}}
                                                        {{--<i class="fa fa-star"></i>--}}
                                                    {{--</span>--}}
													{{--<span>--}}
                                                        {{--<i class="fa fa-star"></i>--}}
                                                    {{--</span>--}}
												{{--</li>--}}
											{{--</ul>--}}
										{{--</li>--}}
									</ul>
								</div>
								<!-- Ends: .product-excerpt -->
							</div>
							<!-- Ends: .product-single -->
						</div>
						@empty
					@endforelse
				</div>
				<div class="text-center m-top-30">
					<a href="{{route('courses')}}" class="btn btn--lg btn-primary">Toutes Nos Formations</a>
				</div>
			</div>
			<!-- Ends: .product-list -->
		</div>
	</div>
</section>
@endif