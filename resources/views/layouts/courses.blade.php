<section class="product-grid p-bottom-100 p-top-100">
	<div class="container">
		@if($series->count())
			<div class="row">
				<div class="col-xl-3 col-lg-4 col-md-12 order-lg-0 order-md-1 order-sm-1 order-1">
					<aside class="sidebar product--sidebar">
						<div class="sidebar-card card--category">
							<a class="card-title" href="#collapse1" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapse1">
								<h5>Categories
									<span class="icon-arrow-down"></span>
								</h5>
							</a>
							<div class="collapse show collapsible-content" id="collapse1">
								<ul class="card-content">
									@foreach($categories as $category)
										<li>
											<a href="#"> {{$category->name}}<span class="item-count">{{$category->series->count()}}</span>
											</a>
										</li>
									@endforeach
								</ul>
							</div>
						</div>
					</aside>
				</div>
				<div class="col-xl-9 col-lg-8 col-md-12 order-lg-1 order-md-0 order-sm-0 order-0 product-list">
					<div class="row">
						@foreach($series as $serie)
							<div class="col-xl-4 col-lg-6 col-md-6">
							<div class="product-single latest-single">
								<div class="product-thumb">
									<figure>
										<img src="{{asset($serie->image_url)}}" alt="" class="img-fluid">
										<figcaption>
											<ul class="list-unstyled">
												<li>
													<a href="{{route('cart.addToCart',['id' => $serie->id])}}">
														<span class="icon-basket"></span>
													</a>
												</li>
												<li>
													<a href="{{route('singleCourse', ['slug' =>$serie->slug])}}">Voir Plus</a>
												</li>
											</ul>
										</figcaption>
									</figure>
								</div>
								<!-- Ends: .product-thumb -->
								<div class="product-excerpt">
									<h5>
										<a href="{{route('singleCourse', ['slug' =>$serie->slug])}}">{{$serie->title}}</a>
									</h5>
									<ul class="titlebtm">
										<li>
											<img class="auth-img" src="images/auth-img2.png" alt="author image">
											<p>
												<a href="#">{{$serie->user->name}}</a>
											</p>
										</li>
										<li class="product_cat">
											dans
											<a href="#">{{$serie->category->name}}</a>
										</li>
									</ul>
									<ul class="product-facts clearfix">
										<li class="price">{{$serie->price}} CFA</li>
										<li class="price">Mode: {{$serie->type()}}</li>
									</ul>
								</div>
							</div>
						</div>
						@endforeach
					</div>
				</div>
				</div>
		@endif
	</div>
</section>
