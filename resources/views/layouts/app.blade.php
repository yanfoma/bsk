<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <!-- viewport meta -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <!-- Favicon Icon -->
        <link rel="icon" type="image/png" sizes="16x16" href="images/favicon-32x32.png">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title')</title>
        @yield('post_header')
        <link href="https://fonts.googleapis.com/css?family=Work+Sans:400,500,600" rel="stylesheet">
        <!-- styles -->
        @include('layouts.styles')
        @yield('style')
    </head>
    <body class="home1 mutlti-vendor">
        @include('layouts.menu')
        @yield('content')
        @include('layouts.footer')
        <!-- Scripts -->
        @include('layouts.scripts')
        @yield('scripts')
    </body>
</html>
