<!-- ================================
Start Services
================================= -->
<section class="services">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-6">
				<div class="service-single">
					<span class="icon-lock"></span>
					<h4>Secure Paument</h4>
					<p>Pellentesque facilisis kamcorper sapien interdum magna.</p>
				</div>
			</div>
			<!-- Ends: .col-md-3 -->
			<div class="col-lg-3 col-md-6">
				<div class="service-single">
					<span class="icon-like"></span>
					<h4>Quality Products</h4>
					<p>Pellentesque facilisis kamcorper sapien interdum magna.</p>
				</div>
			</div>
			<!-- Ends: .col-md-3 -->
			<div class="col-lg-3 col-md-6">
				<div class="service-single">
					<span class="icon-wallet"></span>
					<h4>14 Days Money Backs</h4>
					<p>Pellentesque facilisis kamcorper sapien interdum magna.</p>
				</div>
			</div>
			<!-- Ends: .col-md-3 -->
			<div class="col-lg-3 col-md-6">
				<div class="service-single">
					<span class="icon-support"></span>
					<h4>24/7 Customer Care</h4>
					<p>Pellentesque facilisis kamcorper sapien interdum magna.</p>
				</div>
			</div>
			<!-- Ends: .col-md-3 -->
		</div>
	</div>
</section>
<!-- ================================
End Services
================================= -->