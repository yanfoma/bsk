<section class="cta">
	<div class="container">
		<div class="row">
			<!-- Start Section Title -->
			<div class="col-md-12">
				<div class="section-title">
					<h1>Soyez Acteur de BSK </h1>
				</div>
			</div>
			<!-- Ends: .col-md-12/Section Title -->

			<!-- CTA Single -->
			<div class="col-md-5 cta-single">
				<h3>Offrir Une Formation</h3>
				<p>Vous etes passionne, vous voulez partager votre savoir, n'hesitez pas. Creer votre compte et commencer a former les elites de demain.</p>
				<a href="http://localhost:9000/admin/login" class="btn btn--lg btn-primary">Devenir Formateur</a>
			</div>
			<div class="col-md-2 cta-divider">
				<span>OU</span>
			</div>
			<div class="col-md-5 cta-single">
				<h3>Suivre Une Formation</h3>
				<p>Vous etes interesse par une formation. Alors creer votre compte et commencer a suivre des maintenant</p>
				<a href="{{route('login')}}" class="btn btn--lg btn-secondary">Creer Un Compte</a>
			</div>

		</div>
	</div>
</section>