<!-- ================================
        START MENU AREA
    ================================= -->
<!-- start menu-area -->
<div class="menu-area">
	<div class="top-menu-area">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<div class="menu-fullwidth">
						<div class="logo-wrapper">
							<div class="logo logo-top">
								<a href="{{route('home')}}">BSK Connexion Sarl</a>
							</div>
						</div>
						<div class="menu-container">
							<div class="d_menu">
								<nav class="navbar navbar-expand-lg mainmenu__menu">
									<button class="navbar-toggler" type="button" data-toggle="collapse"
									        data-target="#bs-example-navbar-collapse-1"
									        aria-controls="bs-example-navbar-collapse-1"
									        aria-expanded="false" aria-label="Toggle navigation">
										<span class="navbar-toggler-icon icon-menu"></span>
									</button>
									<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
										<ul class="navbar-nav">
											<li><a href="{{route('home')}}">Accueil</a></li>
											<li><a href="{{route('courses')}}">Tous Les Cours</a></li>
											<li><a href=" http://localhost:9000/admin/login">Devenir Formateur</a></li>
										</ul>
									</div>
								</nav>
							</div>
						</div>

						<div class="author-menu">
							<div class="author-area">
								{{--<div class="search-wrapper">--}}
									{{--<div class="nav_right_module search_module">--}}
										{{--<span class="icon-magnifier search_trigger"></span>--}}
										{{--<div class="search_area">--}}
											{{--<form action="#">--}}
												{{--<div class="input-group input-group-light">--}}
                                                    {{--<span class="icon-left" id="basic-addon7">--}}
                                                        {{--<i class="icon-magnifier"></i>--}}
                                                    {{--</span>--}}
													{{--<input type="text" class="form-control search_field"--}}
													       {{--placeholder="Type words and hit enter...">--}}
												{{--</div>--}}
											{{--</form>--}}
										{{--</div>--}}
									{{--</div>--}}
								{{--</div>--}}
								@guest
								<div class="author__notification_area">
									<ul>
										<li class="has_dropdown">
											<div class="icon_wrap">
												<a href="{{route('cart.index')}}"><span class="icon-basket-loaded"></span></a>
												{{--<span class="notification_count purch">2</span>--}}
											</div>
										</li>
									</ul>
								</div>
									<div class="author__access_area">
										<ul class="d-flex">
											<li><a href="{{route('login')}}">Se Connecter</a></li>
											<li><a href="{{route('register')}}">S'enregistrer</a></li>
										</ul>
									</div>
								@else
									<div class="author__notification_area">
										<ul>
											<li class="has_dropdown">
												<div class="icon_wrap">
													<a href="{{route('cart.index')}}"><span class="icon-basket-loaded"></span></a>
													@if(Cart::content()->count())
														<span class="notification_count purch">{{Cart::content()->count()}}</span>
													@endif
												</div>
											</li>
										</ul>
									</div>
									<!--start .author-author__info-->
									<div class="author-author__info has_dropdown">
										<div class="author__avatar online">
											<img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1539200258/Yanfoma/yanfoshop.png" width="50" alt="user avatar" class="rounded-circle">
										</div>
										<div class="dropdown dropdown--author">
											<div class="author-credits d-flex">
												<div class="author__avatar">
													<img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1539200258/Yanfoma/yanfoshop.png" alt="user avatar"
													     class="rounded-circle">
												</div>
												<div class="autor__info">
													<p class="name">
														{{Auth::user()->name}}
													</p>
												</div>
											</div>
											<ul>
												<li>
													<a href="{{route('student.dashboard')}}">
														<span class="icon-home"></span>Tableau de Bord</a>
												</li>
												<li>
													<a href="{{route('student.dashboard')}}">
														<span class="icon-user"></span>Mes Cours</a>
												</li>
												<li>
													<a href="{{route('student.profile')}}">
														<span class="icon-user"></span>Mon Profile</a>
												</li>
												{{--<li>--}}
													{{--<a href="{{route('cart.index')}}">--}}
														{{--<span class="icon-basket"></span>Mon Panier</a>--}}
												{{--</li>--}}
												<li>
													<a href="{{route('logout')}}">
														<span class="icon-logout"></span>Deconnection</a>
												</li>
											</ul>
										</div>
									</div>
									<!--end /.author-author__info-->
								@endguest
							</div>
							<!-- end .author-area -->

							<!-- author area restructured for mobile -->
							<div class="mobile_content ">
								<span class="icon-user menu_icon"></span>

								<!-- offcanvas menu -->
								<div class="offcanvas-menu closed">
									<span class="icon-close close_menu"></span>
									<div class="author-author__info">
										<div class="author__avatar v_middle">
											<img src="images/user-avater.png" alt="user avatar">
										</div>
									</div>
									<!--end /.author-author__info-->

									<div class="author__notification_area">
										<ul>
											<li>
												<a href="notification.html">
													<div class="icon_wrap">
														<span class="icon-bell"></span>
														<span class="notification_count noti">25</span>
													</div>
												</a>
											</li>

											<li>
												<a href="message.html">
													<div class="icon_wrap">
														<span class="icon-envelope"></span>
														<span class="notification_count msg">6</span>
													</div>
												</a>
											</li>

											<li>
												<a href="cart.html">
													<div class="icon_wrap">
														<span class="icon-basket"></span>
														<span class="notification_count purch">2</span>
													</div>
												</a>
											</li>
										</ul>
									</div>
									<!--start .author__notification_area -->

									<div class="dropdown dropdown--author">
										<ul>
											<li>
												<a href="author.html">
													<span class="icon-user"></span>Profile</a>
											</li>
											<li>
												<a href="dashboard.html">
													<span class="icon-home"></span> Dashboard</a>
											</li>
											<li>
												<a href="dashboard-setting.html">
													<span class="icon-settings"></span> Setting</a>
											</li>
											<li>
												<a href="cart.html">
													<span class="icon-basket"></span>Purchases</a>
											</li>
											<li>
												<a href="favourites.html">
													<span class="icon-heart"></span> Favourite</a>
											</li>
											<li>
												<a href="dashboard-add-credit.html">
													<span class="icon-credit-card"></span>Add Credits</a>
											</li>
											<li>
												<a href="dashboard-statement.html">
													<span class="icon-chart"></span>Sale Statement</a>
											</li>
											<li>
												<a href="dashboard-upload.html">
													<span class="icon-cloud-upload"></span>Upload Item</a>
											</li>
											<li>
												<a href="dashboard-manage-item.html">
													<span class="icon-notebook"></span>Manage Item</a>
											</li>
											<li>
												<a href="dashboard-withdrawal.html">
													<span class="icon-briefcase"></span>Withdrawals</a>
											</li>
											<li>
												<a href="#">
													<span class="icon-logout"></span>Logout</a>
											</li>
										</ul>
									</div>

									<div class="text-center">
										<a href="signup.html" class="author-area__seller-btn inline">Become a Seller</a>
									</div>
								</div>
							</div>
							<!-- end /.mobile_content -->
						</div>
					</div>
				</div>
			</div>
			<!-- end /.row -->
		</div>
		<!-- end /.container -->
	</div>
	<!-- end  -->
</div>
<!-- end /.menu-area -->
<!--================================
    END MENU AREA
    =================================-->