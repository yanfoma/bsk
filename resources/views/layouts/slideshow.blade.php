<!-- Start Intro Area -->
<section class="intro-area bgimage">
	<!-- Start Hero Area -->
	<div class="hero-area hero-area3">
		<!-- start hero-content -->
		<div class="hero-content">
			<!-- start .contact_wrapper -->
			<div class="content-wrapper">
				<!-- start .container -->
				<div class="container">
					<!-- start row -->
					<div class="row">
						<!-- start col-md-12 -->
						<div class="col-md-12">
							<div class="hero__content__title">
								<h1>BSK, La plateforme de formations africaine !!!</h1>
								{{--<p class="tagline">DigiPro is the most powerful, & customizable template for Easy--}}
									{{--Digital Downloads Products</p>--}}
							</div>
							<!-- end .hero__btn-area-->
							<!--start search-area -->
							{{--<div class="search-area">--}}
								{{--<div class="row">--}}
									{{--<div class="col-lg-8 offset-lg-2">--}}
										{{--<!-- start .search_box -->--}}
										{{--<div class="search_box2">--}}
											{{--<form action="#">--}}
												{{--<input type="text" class="text_field"--}}
												       {{--placeholder="votre formation...">--}}
												{{--<button type="submit" class="search-btn btn--lg btn-secondary">Rechercher</button>--}}
											{{--</form>--}}
										{{--</div>--}}
										{{--<!-- end ./search_box -->--}}
									{{--</div>--}}
								{{--</div>--}}
							{{--</div>--}}
							<!--start /.search-area -->
						</div>
						<!-- end /.col-md-12 -->
					</div>
					<!-- end /.row -->
				</div>
				<!-- end /.container -->
			</div>
			<!-- end .contact_wrapper -->
		</div>
		<!-- end hero-content -->
	</div>
	<!-- End Hero Area -->
</section><!-- ends: .intro-area -->
